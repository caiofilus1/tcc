/// <reference types="vite/client" />

interface ImportMetaEnv {
  readonly VITE_BASE_URL: string
  readonly VITE_API_URL: string
  readonly VITE_APP_ROOT: string
  readonly VITE_APP_DO_IMAGE_URL: string
  readonly VITE_APP_DO_NETWORK_URL: string
  readonly VITE_APP_DO_URL: string
  readonly VITE_APP_RECOVER_TOKEN_TIME: string
  readonly VITE_APP_HOUSE_DO_URL: string
  readonly VITE_APP_HOUSE_DO_MAIN: string
  // more env variables...
}

interface ImportMeta {
  readonly env: ImportMetaEnv
}

declare module 'rete-vue-render-plugin' {
  import { Input, Output, Control } from 'rete';

  type InputMapValue = Map<string, Input>['values'];
  type OutputMapValue = Map<string, Output>['values'];
  type ControlMapValue = Map<string, Control>['values'];
  export default {
    name: string,
    install: (context: any, options?: any) => undefined,
    Node: {
      name: string,
      props: ['node', 'editor', 'bindSocket', 'bindControl'],
      components: {
        Socket: { props: ['type', 'socket'], __scopeId: 'data-v-51f75f2c', __file: 'src/Socket.vue' },
      },
      methods: {
        selected: () => string,
        inputs: () => InputMapValue,
        outputs: () => OutputMapValue,
        controls: () => ControlMapValue,
      },
      directives: {
        socket: {}, control: {},
      },
    },
    Socket: {
      props: ['type', 'socket'],
    },
    mixin: {
      name: string,
      props: ['node', 'editor', 'bindSocket', 'bindControl'],
      components: {
        Socket: { props: ['type', 'socket'], __scopeId: 'data-v-51f75f2c', __file: 'src/Socket.vue' },
      },
      methods: {
        selected: () => string,
        inputs: () => InputMapValue,
        outputs: () => OutputMapValue,
        controls: () => ControlMapValue,
      },
      directives: {
        socket: {}, control: {},
      },
    },
    Socket: {
      props: [
        'type',
        'socket',
      ],
    },
  };
}
declare module 'rete-context-menu-plugin'
declare module 'rete-context-menu-plugin'
declare module 'rete-area-plugin'
