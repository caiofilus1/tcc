import 'regenerator-runtime';

import { createApp } from 'vue';
import RippleJs from '@/plugins/ripple';
import vuetify from '@/plugins/vuetify';
import { ipcRenderer } from 'electron';
import { lstat } from 'fs/promises';
import { cwd } from 'process';
import store from './store/index';
import App from './App.vue';
import router from './router/index';

import './services/http/index';

createApp(App)
  .directive('ripple', RippleJs)
  .use(router)
  .use(store)
  .use(vuetify)
  .mount('#app')
  .$nextTick(() => {
    ipcRenderer.on('main-process-message', (_event, ...args) => {
      console.log('[Receive Main-process message]:', ...args);
    });
    lstat(cwd()).then((stats) => {
      console.log('[fs.lstat]', stats);
    }).catch((err) => {
      console.error(err);
    });
  });
