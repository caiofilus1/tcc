import MyNode, { CodeArgs, NodeType } from '@/flow/graph/nodes/MyNode';
import FlowInput from '@/flow/graph/inputs/FlowInput';
import { CodeOutputs, Output } from '@/flow/graph/outputs';
import FlowOutput from '@/flow/graph/outputs/FlowOutput';
import Input from '@/flow/graph/inputs/Input';
import NumberController from '@/flow/graph/controllers/NumberController';
import TextController from '@/flow/graph/controllers/TextController';
import CodeBuilder from '@/flow/codeBuilder/CodeBuilder';
import { Variable } from '@/flow/languages/Variable';
import SelectController from '@/flow/graph/controllers/SelectController';
import ApplicationBuilder from '@/flow/codeBuilder/ApplicationBuilder';

const inputs = {
  flow: new FlowInput(),
};
const outputs = {
  flow: new FlowOutput(),
  value: new Output('valor', 'boolean'),
};
const Controllers = {
  pin: new SelectController('P1', [{ label: 'P1', value: 'P1' }]),
};

export default class ReadPinNode extends MyNode {
  name = 'Ler Entrada';

  type = NodeType.Operation;

  inputs = inputs;

  outputs = outputs;

  controllers = Controllers;

  constructor(readonly applicationBuilder: ApplicationBuilder) {
    super(applicationBuilder);
    this.controllers.pin = new SelectController(
      applicationBuilder.platform.pins[0],
      applicationBuilder.platform.pins.map((item) => ({ label: item, value: item })),
    );
  }

  code({ args, controller }: CodeArgs<typeof inputs, typeof Controllers>): CodeOutputs<typeof outputs> {
    const pin = args.flow.readDigitalPin(controller.pin.value);
    return { flow: args.flow, value: pin as unknown as CodeBuilder };
  }
}
