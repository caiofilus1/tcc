import MyNode, { NodeType } from '@/flow/graph/nodes/MyNode';
import ApplicationBuilder from '@/flow/codeBuilder/ApplicationBuilder';
import { Outputs } from '@/flow/graph/outputs';
import FlowOutput from '@/flow/graph/outputs/FlowOutput';

export default class OnLoopNode extends MyNode {
  name = 'OnLoop';

  type = NodeType.Event;

  outputs: Outputs = {
    flow: new FlowOutput(),
  };

  constructor(readonly applicationBuilder: ApplicationBuilder) {
    super(applicationBuilder);
  }

  // eslint-disable-next-line class-methods-use-this,@typescript-eslint/no-empty-function
  staticCode(): void {

  }

  code() {
    const flow = this.applicationBuilder.getOnLoopFlow();
    return {
      flow,
    };
  }
}
