import MyNode, { CodeArgs, NodeType } from '@/flow/graph/nodes/MyNode';
import FlowInput from '@/flow/graph/inputs/FlowInput';
import { CodeOutputs, Output } from '@/flow/graph/outputs';
import FlowOutput from '@/flow/graph/outputs/FlowOutput';
import Input from '@/flow/graph/inputs/Input';
import NumberController from '@/flow/graph/controllers/NumberController';
import CodeBuilder from '@/flow/codeBuilder/CodeBuilder';
import SelectController from '@/flow/graph/controllers/SelectController';
import { Comparator } from '@/flow/languages/Language';
import ApplicationBuilder from '@/flow/codeBuilder/ApplicationBuilder';
import Platform from '@/flow/Platforms/Platform';

const inputs = {
};
const outputs = {
  onClick: new FlowOutput(),
};

const Controllers = {
  pin: new SelectController('P1', [{ label: 'P1', value: 'P1' }]),
};

export default class OnButtonNode extends MyNode {
  name = 'OnButton';

  type = NodeType.Event;

  inputs = inputs;

  outputs = outputs;

  constructor(readonly applicationBuilder: ApplicationBuilder) {
    super(applicationBuilder);
    this.controllers.pin = new SelectController(
      applicationBuilder.platform.pins[0],
      applicationBuilder.platform.pins.map((item) => ({ label: item, value: item })),
    );
  }

  // eslint-disable-next-line class-methods-use-this,@typescript-eslint/no-empty-function
  staticCode(): void {}

  code({ args, controller }: CodeArgs<typeof inputs, typeof Controllers>): CodeOutputs<typeof outputs> {
    const stateFlow = this.applicationBuilder.addPinListener({
      pin: controller.pin.value,
    });
    return {
      onClick: stateFlow.flow,
    };
  }
}
