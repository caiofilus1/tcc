import MyNode, { CodeArgs, NodeType } from '@/flow/graph/nodes/MyNode';
import FlowInput from '@/flow/graph/inputs/FlowInput';
import { CodeOutputs, Output } from '@/flow/graph/outputs';
import FlowOutput from '@/flow/graph/outputs/FlowOutput';
import Input from '@/flow/graph/inputs/Input';
import NumberController from '@/flow/graph/controllers/NumberController';
import CodeBuilder from '@/flow/codeBuilder/CodeBuilder';
import SelectController from '@/flow/graph/controllers/SelectController';
import { Comparator } from '@/flow/languages/Language';
import ApplicationBuilder from '@/flow/codeBuilder/ApplicationBuilder';
import Platform from '@/flow/Platforms/Platform';

const inputs = {};
const outputs = {
  onRead: new FlowOutput(),
  temperature: new Output('Temperatura', 'number'),
};
const Controllers = {
  pin: new SelectController('P1', [{ label: 'P1', value: 'P1' }]),
};
export default class OnTemperatureNode extends MyNode {
  name = 'Ao Ler Temperatura';

  type = NodeType.Event;

  inputs = inputs;

  outputs = outputs;

  constructor(readonly applicationBuilder: ApplicationBuilder) {
    super(applicationBuilder);
    this.controllers.pin = new SelectController(
      applicationBuilder.platform.analogPins[0],
      applicationBuilder.platform.analogPins.map((item) => ({ label: item, value: item })),
    );
  }

  code({ args, controller }: CodeArgs<typeof inputs, typeof Controllers>): CodeOutputs<typeof outputs> {
    const pinState = this.applicationBuilder.addAnalogPinListener({
      pin: controller.pin.value,
    });
    pinState.flow.delay({ name: 'delay', value: '1000', type: 'number' });
    const ifFlow = pinState.flow.if({
      left: pinState.state,
      right: pinState.oldState,
      comparator: 'different',
    });
    const digitalScale = ifFlow.divide({
      name: 'digitalScale',
      vars: [pinState.state, { name: 'analogScale', type: 'number', value: '1024' }],
    });
    const voltage = ifFlow.multiply({
      name: 'voltage',
      vars: [digitalScale, { name: 'analogScale', type: 'number', value: '5' }],
    });
    const celsius = ifFlow.multiply({
      name: 'celsius',
      vars: [voltage, { name: 'voltagePerCelsius', type: 'number', value: '100' }],
    });
    return {
      onRead: ifFlow,
      temperature: celsius as unknown as CodeBuilder,
    };
  }
}
