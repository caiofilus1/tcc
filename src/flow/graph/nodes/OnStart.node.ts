import MyNode, { CodeArgs, NodeType } from '@/flow/graph/nodes/MyNode';
import ApplicationBuilder from '@/flow/codeBuilder/ApplicationBuilder';
import { CodeOutputs, Outputs } from '@/flow/graph/outputs';
import FlowOutput from '@/flow/graph/outputs/FlowOutput';

const outputs: Outputs = {
  flow: new FlowOutput(),
};

export default class OnLoopNodeOnStartNode extends MyNode {
  name = 'OnStart';

  type = NodeType.Event;

  outputs = outputs;

  constructor(readonly applicationBuilder: ApplicationBuilder) {
    super(applicationBuilder);
  }

  code(): CodeOutputs<typeof outputs> {
    const flow = this.applicationBuilder.getOnStartFlow();
    return {
      flow,
    };
  }
}
