import MyNode, { CodeArgs, NodeType } from '@/flow/graph/nodes/MyNode';
import FlowInput from '@/flow/graph/inputs/FlowInput';
import { CodeOutputs, Output } from '@/flow/graph/outputs';
import FlowOutput from '@/flow/graph/outputs/FlowOutput';
import Input from '@/flow/graph/inputs/Input';
import NumberController from '@/flow/graph/controllers/NumberController';
import TextController from '@/flow/graph/controllers/TextController';
import CodeBuilder from '@/flow/codeBuilder/CodeBuilder';
import { Variable } from '@/flow/languages/Variable';

const inputs = {
  flow: new FlowInput(),
  value: new Input('novo valor', ['number'], { controller: new NumberController(0) }),
};
const outputs = {
  flow: new FlowInput(),
};
const Controllers = {
  name: new TextController(''),
};

export default class SetStateNode extends MyNode {
  name = 'Salve o Estado';

  type = NodeType.State;

  inputs = inputs;

  outputs = outputs;

  controllers = Controllers;

  // eslint-disable-next-line class-methods-use-this,@typescript-eslint/no-empty-function
  staticCode(): void {}

  code({ args, controller }: CodeArgs<typeof inputs, typeof Controllers>): CodeOutputs<typeof outputs> {
    console.log(7888, controller);
    const state = this.applicationBuilder.addState({ name: controller.name.value, value: args.value.name, type: 'number' });
    console.log(123333, { functionName: state.flow.name, args: [args.value] });
    args.flow.procedure({ functionName: state.flow.name, args: [args.value] });
    return { flow: args.flow };
  }
}
