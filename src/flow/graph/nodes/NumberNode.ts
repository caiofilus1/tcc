import MyNode, { CodeArgs, NodeType } from '@/flow/graph/nodes/MyNode';
import FlowInput from '@/flow/graph/inputs/FlowInput';
import { CodeOutputs, Output } from '@/flow/graph/outputs';
import FlowOutput from '@/flow/graph/outputs/FlowOutput';
import Input from '@/flow/graph/inputs/Input';
import NumberController from '@/flow/graph/controllers/NumberController';
import TextController from '@/flow/graph/controllers/TextController';
import CodeBuilder from '@/flow/codeBuilder/CodeBuilder';
import { Variable } from '@/flow/languages/Variable';

const inputs = {
};
const outputs = {
  number: new Output('Número', 'number'),
};
const Controllers = {
  number: new NumberController(0),
};

export default class NumberNode extends MyNode {
  name = 'Number';

  type = NodeType.Operation;

  inputs = inputs;

  outputs = outputs;

  controllers = Controllers;

  // eslint-disable-next-line class-methods-use-this,@typescript-eslint/no-empty-function
  staticCode(): void {}

  code({ args, controller }: CodeArgs<typeof inputs, typeof Controllers>): CodeOutputs<typeof outputs> {
    const variable: Variable = {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      flow: {},
      flows: [] as CodeBuilder[],
      value: controller.number.value,
      type: 'number',
      name: controller.number.value,
    };
    return { number: variable as unknown as CodeBuilder };
  }
}
