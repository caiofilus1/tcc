import MyNode, { CodeArgs, NodeType } from '@/flow/graph/nodes/MyNode';
import ApplicationBuilder from '@/flow/codeBuilder/ApplicationBuilder';
import FlowInput from '@/flow/graph/inputs/FlowInput';
import { CodeInputs, Inputs } from '@/flow/graph/inputs';
import { CodeOutputs, Output } from '@/flow/graph/outputs';
import FlowOutput from '@/flow/graph/outputs/FlowOutput';
import Input from '@/flow/graph/inputs/Input';
import Controller from '@/flow/graph/controllers/Controller';
import SelectController from '@/flow/graph/controllers/SelectController';
import TextController from '@/flow/graph/controllers/TextController';
import { Comparator, Comparators } from '@/flow/languages/Language';

const IfInputs = {
  flow: new FlowInput(),
  a: new Input('a', 'number'),
  b: new Input('b', 'number'),
};
const IfOutputs = {
  then: new FlowOutput('então'),
  else: new FlowOutput('se não'),
};
const IfControllers = {
  comparator: new SelectController<Comparator>('equal', [
    { label: 'é igual', value: 'equal' },
    { label: 'é maior ou igual', value: 'greaterOrEqual' },
    { label: 'é menor ou igual', value: 'greaterOrEqual' },
    { label: 'é menor que', value: 'lessThan' },
    { label: 'é diferente que', value: 'different' },
  ]),
};
export default class IfNode extends MyNode {
  name = 'If';

  type = NodeType.Control;

  inputs = IfInputs;

  outputs = IfOutputs;

  controllers = IfControllers;

  code({ args, controller }: CodeArgs<typeof IfInputs, typeof IfControllers>): CodeOutputs<typeof IfOutputs> {
    const thenFlow = args.flow.if({
      left: args.a,
      right: args.b,
      comparator: controller.comparator.value as Comparator,
    });
    const elseFlow = args.flow.else();
    return { then: thenFlow, else: elseFlow };
  }
}
