import MyNode, { CodeArgs, NodeType } from '@/flow/graph/nodes/MyNode';
import FlowInput from '@/flow/graph/inputs/FlowInput';
import { CodeOutputs, Output } from '@/flow/graph/outputs';
import FlowOutput from '@/flow/graph/outputs/FlowOutput';
import Input from '@/flow/graph/inputs/Input';
import NumberController from '@/flow/graph/controllers/NumberController';
import TextController from '@/flow/graph/controllers/TextController';
import CodeBuilder from '@/flow/codeBuilder/CodeBuilder';
import { Variable } from '@/flow/languages/Variable';

const inputs = {
};
const outputs = {
  value: new Output('valor', 'number'),
};
const Controllers = {
  name: new TextController(''),
};

export default class GetStateNode extends MyNode {
  name = 'Estado';

  type = NodeType.State;

  inputs = inputs;

  outputs = outputs;

  controllers = Controllers;

  code({ args, controller }: CodeArgs<typeof inputs, typeof Controllers>): CodeOutputs<typeof outputs> {
    const state = this.applicationBuilder.addState({ name: controller.name.value, value: '0', type: 'number' });
    return { value: state.state as unknown as CodeBuilder };
  }
}
