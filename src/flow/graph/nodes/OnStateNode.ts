import MyNode, { CodeArgs, NodeType } from '@/flow/graph/nodes/MyNode';
import FlowInput from '@/flow/graph/inputs/FlowInput';
import { CodeOutputs, Output } from '@/flow/graph/outputs';
import FlowOutput from '@/flow/graph/outputs/FlowOutput';
import Input from '@/flow/graph/inputs/Input';
import NumberController from '@/flow/graph/controllers/NumberController';
import TextController from '@/flow/graph/controllers/TextController';
import CodeBuilder from '@/flow/codeBuilder/CodeBuilder';
import { Variable } from '@/flow/languages/Variable';

const inputs = {
};
const outputs = {
  flow: new FlowInput(),
  value: new Output('valor', 'number'),
};
const Controllers = {
  name: new TextController(''),
};

export default class OnStateNode extends MyNode {
  name = 'Ao Mudar o Estado';

  type = NodeType.Event;

  inputs = inputs;

  outputs = outputs;

  controllers = Controllers;

  // eslint-disable-next-line class-methods-use-this,@typescript-eslint/no-empty-function
  staticCode(): void {}

  code({ args, controller }: CodeArgs<typeof inputs, typeof Controllers>): CodeOutputs<typeof outputs> {
    const state = this.applicationBuilder.addState({ name: controller.name.value, value: '0', type: 'number' });
    return { flow: state.flow, value: state.state as unknown as CodeBuilder };
  }
}
