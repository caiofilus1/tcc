import ApplicationBuilder from '@/flow/codeBuilder/ApplicationBuilder';
import { Inputs, CodeInputs } from '@/flow/graph/inputs';
import { CodeOutputs, Outputs } from '@/flow/graph/outputs';
import { CodeController, Controllers } from '@/flow/graph/controllers';
import Platform from '@/flow/Platforms/Platform';

export interface CodeArgs<Input extends Inputs, Controller extends Controllers, > {
    args: CodeInputs<Input>,
    controller: CodeController<Controller>
}

export enum NodeType {
    Default= 'DEFAULT',
    Event = 'EVENT',
    Operation = 'OPERATION',
    Control = 'CONTROL',
    State = 'STATE',
}

export default abstract class MyNode<
        T extends Platform = any,
        L extends Platform['languages'][string] = any,
        ArgDef extends Inputs = Inputs,
        ResultsDef extends Outputs = Outputs,
        ControllerDef extends Controllers = Controllers,
    > {
    abstract name: string;

    type: NodeType = NodeType.Default;

    inputs: ArgDef = {} as ArgDef;

    outputs: ResultsDef = {} as ResultsDef;

    controllers: ControllerDef = {} as ControllerDef;

    constructor(readonly applicationBuilder: ApplicationBuilder) {
    }

    abstract code(variables: CodeArgs<ArgDef, ControllerDef>): CodeOutputs<ResultsDef>;
}
