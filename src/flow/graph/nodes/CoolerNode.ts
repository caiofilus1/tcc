import MyNode, { CodeArgs, NodeType } from '@/flow/graph/nodes/MyNode';
import FlowInput from '@/flow/graph/inputs/FlowInput';
import { CodeOutputs, Output } from '@/flow/graph/outputs';
import FlowOutput from '@/flow/graph/outputs/FlowOutput';
import Input from '@/flow/graph/inputs/Input';
import BooleanController from '@/flow/graph/controllers/BooleanController';
import ApplicationBuilder from '@/flow/codeBuilder/ApplicationBuilder';
import SelectController from '@/flow/graph/controllers/SelectController';
import { Comparator } from '@/flow/languages/Language';

const inputs = {
  flow: new FlowInput(),
  onOff: new Input('Ligar/Desligar', 'boolean', { controller: new BooleanController() }),
};
const outputs = {
  flow: new FlowOutput(),
};
const Controller = {
  pin: new SelectController('P1', [{ label: 'P1', value: 'P1' }]),
};

export default class CoolerNode extends MyNode {
  name = 'Cooler';

  type = NodeType.Operation;

  inputs = inputs;

  outputs = outputs;

  controller = Controller;

  constructor(readonly applicationBuilder: ApplicationBuilder) {
    super(applicationBuilder);
    this.controllers.pin = new SelectController(
      applicationBuilder.platform.pins[0],
      applicationBuilder.platform.pins.map((item) => ({ label: item, value: item })),
    );
  }

  code({ args, controller }: CodeArgs<typeof inputs, typeof Controller>): CodeOutputs<typeof outputs> {
    this.applicationBuilder.registerPin(controller.pin.value, 'OUT');
    args.flow.writeDigitalPin(controller.pin.value, args.onOff);
    return { flow: args.flow };
  }
}
