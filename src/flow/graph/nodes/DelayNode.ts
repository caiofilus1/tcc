import MyNode, { CodeArgs, NodeType } from '@/flow/graph/nodes/MyNode';
import FlowInput from '@/flow/graph/inputs/FlowInput';
import { CodeOutputs, Output } from '@/flow/graph/outputs';
import FlowOutput from '@/flow/graph/outputs/FlowOutput';
import Input from '@/flow/graph/inputs/Input';
import BooleanController from '@/flow/graph/controllers/BooleanController';
import ApplicationBuilder from '@/flow/codeBuilder/ApplicationBuilder';
import SelectController from '@/flow/graph/controllers/SelectController';
import { Comparator } from '@/flow/languages/Language';
import NumberController from '@/flow/graph/controllers/NumberController';

const inputs = {
  flow: new FlowInput(),
};
const outputs = {
  flow: new FlowOutput(),
};
const Controller = {
  time: new NumberController(1),
};

export default class DelayNode extends MyNode {
  name = 'Espere';

  type = NodeType.Operation;

  inputs = inputs;

  outputs = outputs;

  controllers = Controller;

  constructor(readonly applicationBuilder: ApplicationBuilder) {
    super(applicationBuilder);
  }

  code({ args, controller }: CodeArgs<typeof inputs, typeof Controller>): CodeOutputs<typeof outputs> {
    console.log(controller.time);
    args.flow.delay(controller.time);
    return { flow: args.flow };
  }
}
