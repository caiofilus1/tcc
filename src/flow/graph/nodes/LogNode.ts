import MyNode, { CodeArgs, NodeType } from '@/flow/graph/nodes/MyNode';
import FlowInput from '@/flow/graph/inputs/FlowInput';
import { CodeOutputs, Output } from '@/flow/graph/outputs';
import FlowOutput from '@/flow/graph/outputs/FlowOutput';
import Input from '@/flow/graph/inputs/Input';
import NumberController from '@/flow/graph/controllers/NumberController';
import TextController from '@/flow/graph/controllers/TextController';

const inputs = {
  flow: new FlowInput(),
  text: new Input('texto', ['string', 'number', 'boolean'], { controller: new TextController() }),
};
const outputs = {
  flow: new FlowOutput(),
};

export default class LogNode extends MyNode {
  name = 'Log';

  type = NodeType.Operation;

  inputs = inputs;

  outputs = outputs;

  // eslint-disable-next-line class-methods-use-this,@typescript-eslint/no-empty-function
  staticCode(): void {}

  code({ args }: CodeArgs<typeof inputs, typeof outputs>): CodeOutputs<typeof outputs> {
    args.flow.log(args.text);
    this.applicationBuilder.getOnStartFlow().initLog(9600);
    return { flow: args.flow };
  }
}
