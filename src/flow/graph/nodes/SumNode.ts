import MyNode, { CodeArgs, NodeType } from '@/flow/graph/nodes/MyNode';
import FlowInput from '@/flow/graph/inputs/FlowInput';
import { CodeOutputs, Output } from '@/flow/graph/outputs';
import FlowOutput from '@/flow/graph/outputs/FlowOutput';
import Input from '@/flow/graph/inputs/Input';
import NumberController from '@/flow/graph/controllers/NumberController';
import CodeBuilder from '@/flow/codeBuilder/CodeBuilder';

const inputs = {
  flow: new FlowInput(),
  a: new Input('A', 'number', { controller: new NumberController() }),
  b: new Input('B', 'number', { controller: new NumberController() }),
};
const outputs = {
  flow: new FlowOutput(''),
  sum: new Output('resultado', 'number'),
};

export default class SumNode extends MyNode {
  name = 'Sum';

  type = NodeType.Operation;

  inputs = inputs;

  outputs = outputs;

  // eslint-disable-next-line class-methods-use-this,@typescript-eslint/no-empty-function
  staticCode(): void {}

  code({ args, controller }: CodeArgs<typeof inputs, typeof outputs>): CodeOutputs<typeof outputs> {
    const sumVar = args.flow.sum({
      name: 'sum',
      vars: [args.a, args.b],
    });
    return { flow: args.flow, sum: sumVar as unknown as CodeBuilder };
  }
}
