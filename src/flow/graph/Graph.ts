import CodeBuilder from '@/flow/codeBuilder/CodeBuilder';
import Node from '@/flow/graph/nodes/MyNode';
import Platform from '@/flow/Platforms/Platform';

export default class Graph<P extends Platform> {
  private platform!: P;

  private language!: P['languages'][string];

  private codeBuilder!: CodeBuilder<P, P['languages'][string]>;

  private graphs: Node[] = [];

  private nodes: Node[] = [];

  constructor(platform: P, language: P['languages'][string]) {
    this.platform = platform;
    this.language = language;
    this.codeBuilder = new CodeBuilder<P, P['languages'][string]>(platform, this.language);
  }

  addNode(node: Node) {
    this.nodes.push(node);
  }

  addRemove(node: Node) {
    this.nodes.splice(this.nodes.indexOf(node));
  }

  connect(node1: Node, node2: Node) {
    this.nodes;
  }
}
