import Output from '@/flow/graph/outputs/Output';
import CodeBuilder from '@/flow/codeBuilder/CodeBuilder';
import FlowOutput from '@/flow/graph/outputs/FlowOutput';
import { Variable } from '@/flow/languages/Variable';

export { default as Output } from './Output';

export type Outputs = {
    [key: string]: Output | FlowOutput,
}

export type CodeOutputs<T extends Outputs> = {
    [Key in keyof T]: T[Key] extends FlowOutput ? CodeBuilder: Variable;
}
