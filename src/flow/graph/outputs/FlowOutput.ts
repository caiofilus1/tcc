export default class FlowOutput {
  type = 'flow';

  label: string;

  constructor(label = '') {
    this.label = label;
  }
}
