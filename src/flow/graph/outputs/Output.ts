import { Types } from '@/flow/languages/LanguageTypes';

export default class Output {
  constructor(readonly label: string, readonly type: Types) {
  }
}
