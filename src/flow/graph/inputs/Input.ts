import Controller from '@/flow/graph/controllers/Controller';
import { Types } from '@/flow/languages/LanguageTypes';

interface InputOpts<T extends Types> {
    controller?: Controller;
}

export default class Input<T extends Types = Types> {
  controller?: Controller;

  constructor(label: string, readonly type: T | T[], opts?: InputOpts<T>) {
    if (opts) {
      this.controller = opts.controller;
    }
  }
}
