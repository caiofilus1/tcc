export default class FlowInput {
  type = 'flow' as const;

  constructor(readonly label: string = '') {
  }
}
