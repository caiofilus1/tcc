import FlowInput from '@/flow/graph/inputs/FlowInput';

import CodeBuilder from '@/flow/codeBuilder/CodeBuilder';
import Input from '@/flow/graph/inputs/Input';
import {Constant, Variable} from '@/flow/languages/Variable';
import Controller from "@/flow/graph/controllers/Controller";

export interface Inputs {
    [key: string]: Input | FlowInput
}

export type CodeInputs<T extends Inputs> = {
    [Key in keyof T]: T[Key] extends FlowInput
        ? CodeBuilder
        : T[Key] extends {controller: Controller}
            ? Variable | Constant
            : Variable;
}
