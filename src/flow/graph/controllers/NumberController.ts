import Controller from '@/flow/graph/controllers/Controller';

export default class NumberController extends Controller {
  constructor(readonly defaultValue?: number) {
    super();
  }
}
