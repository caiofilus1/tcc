import Controller from '@/flow/graph/controllers/Controller';
import { Comparator } from '@/flow/languages/Language';

export default class SelectController<T> extends Controller {
  constructor(readonly type: T, readonly items?: { label: string, value: T }[]) {
    super();
  }
}
