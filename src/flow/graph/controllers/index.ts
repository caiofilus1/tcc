import Controller from '@/flow/graph/controllers/Controller';
import {Constant} from "@/flow/languages/Variable";

export type Controllers = {
    [key: string]: Controller
}

export type CodeController<T extends Controllers> = {
    [Key in keyof T]: Constant;
}
