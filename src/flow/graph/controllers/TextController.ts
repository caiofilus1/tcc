import Controller from '@/flow/graph/controllers/Controller';

export default class TextController extends Controller {
  constructor(readonly defaultValue: string = '') {
    super();
  }
}
