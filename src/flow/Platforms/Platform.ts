import { FlowTemplate } from '@/flow/languages/Language';

export default interface Platform {
    name: string,
    compiler: string,
    pins: string[],
    analogPins: string[]
    timers: string[]
    languages: {[key: string]: FlowTemplate}
}
