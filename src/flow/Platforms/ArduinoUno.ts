import Platform from '@/flow/Platforms/Platform';
import CppTemplate from '@/flow/languages/CPlusPlus';
import { WriteDigitalPinProp } from '@/flow/languages/Language';

class ArduinoCpp extends CppTemplate {
  print(s: string): string {
    return `Serial.println(${s});`;
  }

  initPrint(opts: {speed: number}): string {
    return `Serial.begin(${opts.speed});`;
  }

  registerPin(opts: { pin: Platform['pins'][number]; direction: 'IN' | 'OUT' }): string {
    return `pinMode(${opts.pin}, ${opts.direction === 'IN' ? 'INPUT' : 'OUTPUT'});`;
  }

  readDigitalPin(pin: string): string {
    return `digitalRead(${pin})`;
  }

  readAnalogPin(pin: string): string {
    return `analogRead(${pin})`;
  }

  writeDigitalPin(props: WriteDigitalPinProp): string {
    return `digitalWrite(${props.pin}, ${props.value ? 'true' : 'false'});`;
  }

  setPin = (pin: string, value: 'IN' | 'OUT'): string => `pinMode(${pin}, ${value});`;

  delay(millis: number): string {
    return `delay(${millis});`;
  }
}

export class ArduinoUno implements Platform {
  name = 'Arduino Uno';

  compiler = 'arduino-cli';

  pins = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13'];

  analogPins = ['A0', 'A1', 'A2', 'A3', 'A4', 'A5'];

  timers = ['T0', 'T1'];

  languages = {
    cpp: new ArduinoCpp(),
  };
}
export class ArduinoNano implements Platform {
  name = 'Arduino Nano';

  compiler = 'a';

  pins = ['A0', 'A1', 'A2', 'D0', 'D1', 'D2', 'D3'];

  analogPins = ['A0', 'A1', 'A2', 'A3', 'A4', 'A5'];

  timers = ['T0', 'T1'];

  languages = {
    cpp: new ArduinoCpp(),
  };
}
