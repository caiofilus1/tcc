import * as Rete from 'rete';
import { Component, Node, NodeEditor } from 'rete';
import {
  booleanSocket, numberSocket, stringSocket, voidSocket, numberStringBooleanSocket,
} from '@/flow/sockets/number.socket';
import MyNode from '@/flow/graph/nodes/MyNode';
import { flowSocket } from '@/flow/sockets/generic.socket';
import { NodeData, WorkerInputs, WorkerOutputs } from 'rete/types/core/data';
import { CodeInputs, Inputs } from '@/flow/graph/inputs';
import { CodeController, Controllers } from '@/flow/graph/controllers';
import SelectController from '@/flow/graph/controllers/SelectController';
import NumberController from '@/flow/graph/controllers/NumberController';
import TextController from '@/flow/graph/controllers/TextController';
import ReteNumberControl from '@/flow/controlers/ReteNumberControl';
import ReteSelectControl from '@/flow/controlers/ReteSelectControl';
import ReteTextControl from '@/flow/controlers/ReteTextControl';
import Controller from '@/flow/graph/controllers/Controller';
import FlowInput from '@/flow/graph/inputs/FlowInput';
import CustomNode from '@/flow/components/CustomNode.vue';
import { Types } from '@/flow/languages/LanguageTypes';
import { Constant, Variable } from '@/flow/languages/Variable';
import CodeBuilder from '@/flow/codeBuilder/CodeBuilder';
import BooleanController from '@/flow/graph/controllers/BooleanController';
import ReteBooleanControl from '@/flow/controlers/ReteBooleanControl';

const socketMap: Record<Types | 'flow', Rete.Socket> = {
  flow: flowSocket,
  boolean: booleanSocket,
  number: numberSocket,
  string: stringSocket,
  void: voidSocket,
};

function convertWorkerInputsToArgs<T extends Inputs, A extends CodeInputs<T>>(node: NodeData, inputs: WorkerInputs, args: T): A {
  const entries = Object.entries(args).map(([key, arg]) => {
    if (inputs?.[key]?.[0]) return [key, inputs[key][0]];
    const value = node.data[key];
    if (!value) return [key, undefined];
    const constant: Constant = {
      name: key,
      type: 'string',
      value: value as string,
    };
    return [key, constant];
  });
  return Object.fromEntries(entries);
}
function convertDataToController<T extends Controllers, A extends CodeController<T>>(data: NodeData['data'], controllers: T): A {
  const entries = Object
    .entries(controllers)
    .map(([key, arg]) => [key, { name: key, type: 'string', value: data[key] }]);
  return Object.fromEntries(entries);
}

function inferSocketType(type: Types | Types[] | 'flow') {
  if (Array.isArray(type)) {
    if (type.includes('string') && type.includes('boolean') && type.includes('number')) return numberStringBooleanSocket;
  } else {
    return socketMap[type];
  }
  return numberSocket;
}

export default function adapterReteNode(myNode: MyNode): Component {
  const Klass = class extends Component {
    constructor() {
      super(myNode.name);
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      this.data.component = CustomNode;

      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      this.data.props = {
        type: myNode.type,
      };
    }

    async builder(node: Node) {
      Object.entries(myNode.inputs).forEach(([key, value]) => {
        const type = value.type as Types | Types[] | 'flow';
        const label = 'label' in value ? value.label as string : key;
        const input = new Rete.Input(key, label, inferSocketType(type), false);
        if ('controller' in value && value.controller) {
          const controller = value.controller as Controller;
          if (controller instanceof SelectController) {
            const options = (controller.items || []).map((item) => ({ label: item.label, value: String(item.value) }));
            input.addControl(new ReteSelectControl(this.editor as NodeEditor, key, { items: options }));
          } else if (controller instanceof NumberController) {
            input.addControl(new ReteNumberControl(this.editor as NodeEditor, key, { defaultValue: controller.defaultValue }));
          } else if (controller instanceof TextController) {
            input.addControl(new ReteTextControl(this.editor as NodeEditor, key));
          } else if (controller instanceof BooleanController) {
            input.addControl(new ReteBooleanControl(this.editor as NodeEditor, key));
          }
        }
        node.addInput(input);
      });
      Object.entries(myNode.outputs).forEach(([key, value]) => {
        const type = value.type as Types | 'flow';
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        const input = new Rete.Output(key, value.label, socketMap[type], type !== 'flow ');
        node.addOutput(input);
      });
      Object.entries(myNode.controllers).forEach(([key, value]) => {
        if (value instanceof SelectController) {
          const options = (value.items || []).map((item) => ({ label: item.label, value: String(item.value) }));

          node.addControl(new ReteSelectControl(this.editor as NodeEditor, key, { items: options, defaultValue: value.type }));
        } else if (value instanceof NumberController) {
          node.addControl(new ReteNumberControl(this.editor as NodeEditor, key, { defaultValue: value.defaultValue }));
        } else if (value instanceof TextController) {
          node.addControl(new ReteTextControl(this.editor as NodeEditor, key));
        } else if (value instanceof BooleanController) {
          node.addControl(new ReteBooleanControl(this.editor as NodeEditor, key));
        }
      });
    }

    worker(node: NodeData, inputs: WorkerInputs, outputs: WorkerOutputs): void {
      const args = convertWorkerInputsToArgs(node, inputs, myNode.inputs);
      const controllers = convertDataToController(node.data, myNode.controllers);
      if (!Object.values(args).every((item) => !!item)) return;
      const result = myNode.code({ args, controller: controllers });
      // eslint-disable-next-line no-param-reassign
      Object.entries(result).forEach(([key, value]) => outputs[key] = value);
    }
  };
  return new Klass();
}
