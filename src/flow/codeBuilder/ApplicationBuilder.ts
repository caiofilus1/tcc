import CodeBuilder, { FunctionBuilder } from '@/flow/codeBuilder/CodeBuilder';
import Platform from '@/flow/Platforms/Platform';
import { Comparators, VarProps } from '@/flow/languages/Language';
import { Constant, Variable } from '@/flow/languages/Variable';
import { Types } from '@/flow/languages/LanguageTypes';

interface State <
    T extends Platform,
    L extends Platform['languages'][string] >{
    state: Variable,
    oldState: Variable,
    flow: FunctionBuilder<T, L>
}

export default class ApplicationBuilder<
    T extends Platform = Platform,
    L extends Platform['languages'][string] = T['languages'][string]> {
  states: Map<string, State<T, L>> = new Map<string, State<T, L>>();

  headers: CodeBuilder<T, L>;

  static: CodeBuilder<T, L>;

  loop: CodeBuilder<T, L>;

  main: CodeBuilder<T, L>;

  eventHandler: CodeBuilder<T, L>;

  handlePortsFunction: CodeBuilder<T, L>;

  pinsRegister: CodeBuilder<T, L>;

  private onStart: CodeBuilder<T, L>;

  private kernel: CodeBuilder<T, L>;

  usedPins: {pin: T['pins'][number], type: 'IN' | 'OUT', label: string}[] = [];

  get freePins() {
    const usedPins = this.usedPins.map((item) => item.pin);
    return this.platform.pins.filter((item) => !usedPins.includes(item));
  }

  constructor(readonly platform: T, readonly language: L) {
    this.headers = new CodeBuilder(platform, language);
    this.static = new CodeBuilder(platform, language);
    this.kernel = new CodeBuilder(platform, language);
    this.onStart = new CodeBuilder(platform, language);
    this.loop = new CodeBuilder(platform, language);
    this.eventHandler = new CodeBuilder(platform, language);
    this.main = new CodeBuilder(platform, language);
    this.handlePortsFunction = new CodeBuilder(platform, language);
    this.pinsRegister = new CodeBuilder(platform, language);
    this.initBuilders();
  }

  addPinListener(props: {pin: Platform['pins'][number], }) {
    const stateFlow = this.addState({
      name: `pin${props.pin}State`,
      value: 'false',
      type: 'boolean',
    });
    this.usedPins.push({ pin: props.pin, type: 'IN', label: 'pin' });
    const newPinValue = this.handlePortsFunction.readDigitalPin(props.pin);
    const ifChangeState = this.handlePortsFunction.if({ left: newPinValue, comparator: 'different', right: stateFlow.state });
    ifChangeState.procedure({ functionName: `setPin${props.pin}State`, args: [newPinValue] });
    return stateFlow;
  }

  addAnalogPinListener(props: {pin: Platform['pins'][number], }) {
    this.registerPin(props.pin, 'IN');
    const stateFlow = this.addState({
      name: `pin${props.pin}State`,
      value: '0',
      type: 'number',
    });
    const newPinValue = this.handlePortsFunction.readAnalogPin(props.pin);
    this.handlePortsFunction.procedure({ functionName: `setPin${props.pin}State`, args: [newPinValue] });
    return stateFlow;
  }

  addState(props: Required<VarProps>): State<T, L> {
    const localState = this.states.get(props.name);
    if (localState) return localState;

    const stateVar = this.static.var(props);
    const oldStateVar = this.static.var({ ...props, name: `${props.name}Old` });
    const stateFlow = this.static.function(
      {
        name: `set${props.name.substring(0, 1).toUpperCase()}${props.name.substring(1)}`,
        returnType: 'void',
        args: [{ name: 'newState', type: props.type }],
      },
    );

    const newStateArg = stateFlow.args[0];
    stateFlow.assing(oldStateVar, stateVar);
    stateFlow.assing(stateVar, newStateArg);
    const state: State<T, L> = { state: stateVar, oldState: oldStateVar, flow: stateFlow };
    this.states.set(props.name, state);
    return state;
  }

  getOnStartFlow() {
    return this.onStart;
  }

  getOnLoopFlow() {
    return this.loop;
  }

  getOnStateChangeFlow(name: string) {
    const state = this.states.get(name);
    if (!state) throw new Error('State not Found');
    return state.flow;
  }

  build() {
    return [this.headers.build(), this.static.build(), this.main.build()].join('\n');
  }

  clear() {
    const builders = [this.headers, this.static, this.main];
    builders.forEach((builder) => builder.clear());
    this.usedPins = [];
    this.states.clear();
    this.initBuilders();
  }

  registerPin(pin: Platform['pins'][number], direction: 'IN' | 'OUT') {
    this.pinsRegister.registerPin({ pin, direction });
  }

  private initBuilders() {
    this.handlePortsFunction = this.static.function({
      name: 'handlePorts',
      returnType: 'void',
      args: [],
    });
    this.pinsRegister = this.main.function({ name: 'registerPins', returnType: 'void', args: [] });
    const mainFunction = this.main.function({ name: 'setup', returnType: 'void', args: [] });
    this.main.function({ name: 'loop', returnType: 'void', args: [] });
    mainFunction.procedure({ functionName: 'registerPins', args: [] });
    this.kernel = mainFunction.addFragment();
    this.onStart = mainFunction.addFragment();

    this.loop = mainFunction.addFragment().while({ right: 'true', comparator: 'equal', left: 'true' });
    this.loop.procedure({ functionName: 'handlePorts', args: [] });
  }
}
