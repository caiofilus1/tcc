// eslint-disable-next-line max-classes-per-file,import/no-cycle
import {
  Arg,
  CallProps,
  Comparators,
  FunctionProps,
  IfProps,
  ProcedureProps,
  VarProps,
  WhileProps,
} from '@/flow/languages/Language';
import Platform from '@/flow/Platforms/Platform';
// eslint-disable-next-line import/no-cycle
import { Constant, Variable } from '@/flow/languages/Variable';
import { Types } from '@/flow/languages/LanguageTypes';

export interface Fragment {
  name?: string,
  deep: number,
  build(): string,
}

export class CodeFragment<A extends object | string, T extends (...args: any) => any> implements Fragment {
  constructor(readonly builder: T, readonly props: A, readonly deep = 0) {
  }

  build() {
    return this.builder(this.props);
  }
}

export default class CodeBuilder<
    T extends Platform = Platform,
    L extends Platform['languages'][string] = T['languages'][string]
    >
implements Fragment {
  codeFragment: Fragment[] = [];

  deep = 0;

  // eslint-disable-next-line no-use-before-define
  pastFlows: CodeBuilder[] = [];

  constructor(readonly platform: T, readonly language: L, opts?: {deep?: number, pastFlows: CodeBuilder[]}) {
    if (opts?.deep) this.deep = opts.deep;
  }

  addFragment() {
    const builder = new CodeBuilder(this.platform, this.language);
    this.codeFragment.push(builder);
    return builder;
  }

  var(props: VarProps): Variable {
    const name = `${props.name}${Date.now()}`;
    this.codeFragment.push(new CodeFragment(this.language.varTemplate.bind(this.language), { ...props, name }));
    return {
      flow: this,
      flows: this.pastFlows.concat(this),
      name,
      type: props.type,
    };
  }

  assing<C extends Variable>(props: C, value: Variable | Constant) {
    this.codeFragment.push(
      new CodeFragment(
        this.language.varTemplate.bind(this.language),
        { type: '', name: props.name, value: 'flow' in value ? value.name : value.value },
      ),
    );
    return props;
  }

  sum(props: { name: string, vars: (Variable | Constant)[] }): Variable {
    const name = `${props.name}_${Date.now()}`;
    const addCode = props.vars
      .map((item) => ('flow' in item ? item.name : item.value))
      .join(this.language.operations.sum);
    this.codeFragment.push(
      new CodeFragment(this.language.varTemplate.bind(this.language), {
        type: 'number',
        name,
        value: addCode,
      }),
    );
    return {
      flow: this,
      flows: this.pastFlows.concat(this),
      name,
      type: 'number',
    };
  }

  multiply(props: { name: string, vars: (Variable | Constant)[] }): Variable {
    const name = `${props.name}_${Date.now()}`;
    const code = props.vars
      .map((item) => ('flow' in item ? item.name : item.value))
      .join(this.language.operations.multiply);
    this.codeFragment.push(
      new CodeFragment(this.language.varTemplate.bind(this.language), {
        type: 'number',
        name,
        value: code,
      }),
    );
    return {
      flow: this,
      flows: this.pastFlows.concat(this),
      name,
      type: 'number',
    };
  }

  divide(props: { name: string, vars: [(Variable | Constant), (Variable | Constant)] }): Variable {
    const name = `${props.name}_${Date.now()}`;
    const code = props.vars
      .map((item) => ('flow' in item ? item.name : item.value))
      .join(this.language.operations.divide);
    this.codeFragment.push(
      new CodeFragment(this.language.varTemplate.bind(this.language), {
        type: 'number',
        name,
        value: code,
      }),
    );
    return {
      flow: this,
      flows: this.pastFlows.concat(this),
      name,
      type: 'number',
    };
  }

  if(props: {
    left: Variable | string,
    right: Variable | string,
    comparator: keyof Comparators
  }) {
    // eslint-disable-next-line no-use-before-define
    const builder: IfBuilder = new IfBuilder(this.platform, this.language, {
      left: typeof props.left === 'string' ? props.left : props.left.name,
      right: typeof props.right === 'string' ? props.right : props.right.name,
      comparator: props.comparator,
    }, this);
    this.codeFragment.push(builder);
    return builder;
  }

  else() {
    // eslint-disable-next-line no-use-before-define
    const builder = new ElseBuilder(this.platform, this.language, this);
    this.codeFragment.push(builder);
    return builder;
  }

  // eslint-disable-next-line no-use-before-define
  function<Ty extends Types>(props: Omit<FunctionProps<Ty>, 'code'>): FunctionBuilder<T, L, Ty> {
    // eslint-disable-next-line no-use-before-define
    const builder = new FunctionBuilder(this.platform, this.language, props, this);
    this.codeFragment.push(builder);
    return builder;
  }

  call(props: CallProps) {
    this.codeFragment.push(new CodeFragment(this.language.callTemplate.bind(this.language), props));
    return this;
  }

  procedure(props: ProcedureProps) {
    this.codeFragment.push(new CodeFragment(this.language.callProcedureTemplate.bind(this.language), props));
    return this;
  }

  while(props: Omit<WhileProps, 'code'>) {
    // eslint-disable-next-line no-use-before-define
    const builder: WhileBuilder = new WhileBuilder(this.platform, this.language, props, this);
    this.codeFragment.push(builder);
    return builder;
  }

  log(s: Variable | Constant) {
    this.codeFragment.push(
      new CodeFragment(
        this.language.print.bind(this.language),
        'value' in s ? s.value : s.name,
      ),
    );
  }

  initLog(speed: number) {
    this.codeFragment.push(
      new CodeFragment(this.language.initPrint, { speed }),
    );
  }

  registerPin(opts: { pin: Platform['pins'][number], direction: 'IN' | 'OUT'}) {
    this.codeFragment.push(
      new CodeFragment(this.language.registerPin, opts),
    );
  }

  readDigitalPin(pin: Platform['pins'][number]) {
    const variable = this.var({
      name: `pin${pin}`,
      type: 'boolean',
      value: this.language.readDigitalPin(pin),
    });
    return variable;
  }

  readAnalogPin(pin: Platform['analogPins'][number]) {
    const variable = this.var({
      name: `pin${pin}`,
      type: 'number',
      value: this.language.readAnalogPin(pin),
    });
    return variable;
  }

  writeDigitalPin(pin: Platform['pins'][number], value: Variable | Constant) {
    return this.codeFragment.push(
      new CodeFragment(this.language.writeDigitalPin, { pin, value: 'value' in value ? value.value === 'true' : value.name }),
    );
  }

  delay(millis: Constant | Variable) {
    return this.codeFragment.push(new CodeFragment(this.language.delay, 'value' in millis ? millis.value : millis.name));
  }

  clear() {
    this.codeFragment.splice(0);
  }

  build() {
    return this.codeFragment.map((builder) => builder.build()).join('\n');
  }
}

export class FunctionBuilder<
    T extends Platform = any,
    L extends Platform['languages'][string] = any,
    FuncType extends Types = Types
    > extends CodeBuilder<T, L> implements Fragment {
  name: string;

  args: Array<Variable>;

  constructor(
      readonly platform: T,
      readonly language: L,
      readonly props: Omit<FunctionProps<FuncType>, 'code'>,
      readonly parent?: CodeBuilder<T, L>,
  ) {
    super(platform, language);
    this.name = props.name;
    this.args = props.args.map((item): Variable => (
      {
        name: item.name,
        type: item.type,
        flow: parent?.pastFlows[0] || undefined as unknown as CodeBuilder,
        flows: [],
      }));
  }

  build(): string {
    const functiontemplate = new CodeFragment(this.language.functionTemplate.bind(this.language), {
      returnType: this.props.returnType,
      name: this.props.name,
      args: this.props.args,
      code: super.build(),
    });
    return functiontemplate.build();
  }

  return(value: string) {
    this.codeFragment.push(new CodeFragment(this.language.returnTemplate.bind(this.language), { value }));
    return this.parent;
  }
}

export class IfBuilder <
    T extends Platform = any,
    L extends Platform['languages'][string] = any,
    FuncType extends Types = Types
    > extends CodeBuilder<T, L> implements Fragment {
  constructor(
      readonly platform: T,
      readonly language: L,
      readonly props: Omit<IfProps, 'code'>,
      readonly parent?: CodeBuilder<T, L>,
  ) {
    super(platform, language);
  }

  build(): string {
    const functionTemplate = new CodeFragment(this.language.ifTemplate.bind(this.language), {
      left: this.props.left,
      right: this.props.right,
      comparator: this.props.comparator,
      code: super.build().split('\n').join('\n  '),
    });
    return functionTemplate.build();
  }

  return() {
    return this.parent;
  }
}

export class ElseBuilder <
    T extends Platform = any,
    L extends Platform['languages'][string] = any,
    FuncType extends Types = Types
    > extends CodeBuilder<T, L> implements Fragment {
  constructor(
      readonly platform: T,
      readonly language: L,
      readonly parent?: CodeBuilder<T, L>,
  ) {
    super(platform, language);
  }

  build(): string {
    const functionTemplate = new CodeFragment(this.language.elseTemplate.bind(this.language), {
      code: super.build().split('\n').join('\n  '),
    });
    return functionTemplate.build();
  }

  return() {
    return this.parent;
  }
}

export class WhileBuilder <
    T extends Platform = any,
    L extends Platform['languages'][string] = any,
    FuncType extends Types = Types
    > extends CodeBuilder<T, L> implements Fragment {
  constructor(
      readonly platform: T,
      readonly language: L,
      readonly props: Omit<WhileProps, 'code'>,
      readonly parent?: CodeBuilder<T, L>,
  ) {
    super(platform, language);
  }

  build(): string {
    const functionTemplate = new CodeFragment(this.language.whileTemplate.bind(this.language), {
      left: this.props.left,
      right: this.props.right,
      comparator: this.props.comparator,
      code: super.build().split('\n').join('\n  '),
    });
    return functionTemplate.build();
  }

  return() {
    return this.parent;
  }
}
