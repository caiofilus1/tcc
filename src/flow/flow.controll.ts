import * as Rete from 'rete';
import ConnectionPlugin from 'rete-connection-plugin';
import VueRenderPlugin from 'rete-vue-render-plugin';
import { Component } from 'rete';
import DragPlugin from '@/flow/plugins/dragPlugin';
// eslint-disable-next-line import/no-cycle
import MyNode, { CodeArgs } from '@/flow/graph/nodes/MyNode';
import adapterReteNode from '@/flow/adapters/ReteNodeAdapter';
import ApplicationBuilder from '@/flow/codeBuilder/ApplicationBuilder';
import Platform from '@/flow/Platforms/Platform';

const flowMap: string[] = [];
export default flowMap;

export class FlowControl<P extends Platform, L extends P['languages'][string]> {
  nodeOrder: string[] = [];

  code = '';

  editor!: Rete.NodeEditor;

  engine!: Rete.Engine;

  applicationBuilder!: ApplicationBuilder<P, L>;

  listeners: ((code: string) => void)[] = [];

  constructor(readonly platform: P, readonly language: L, container: HTMLElement) {
    const editor = new Rete.NodeEditor('demo@0.1.0', container);
    const engine = new Rete.Engine('demo@0.1.0');

    this.applicationBuilder = new ApplicationBuilder(platform, language);

    editor.use(ConnectionPlugin);
    editor.use(VueRenderPlugin);

    editor.use(DragPlugin, {
      container: document.querySelector('.dock'),
      itemClass: 'item', // default: dock-item
      plugins: [VueRenderPlugin], // render plugins
    } as unknown as void);

    editor.on(['process', 'nodecreated', 'noderemoved', 'connectioncreated', 'connectionremoved'], async () => {
      flowMap.splice(0, flowMap.length);
      this.applicationBuilder.clear();
      await engine.abort();
      await engine.process(editor.toJSON());
      const code = this.applicationBuilder.build();

      this.listeners.forEach((fn) => fn(code));
    });

    this.editor = editor;
    this.engine = engine;
  }

  // eslint-disable-next-line class-methods-use-this,@typescript-eslint/no-empty-function
  addComponent() {

  }

  init() {
    // eslint-disable-next-line no-unused-expressions
    this;
  }

  registerComponent(node: new (...args: any[]) => MyNode) {
    // eslint-disable-next-line new-cap
    const reteComponent = adapterReteNode(new node(this.applicationBuilder));
    this.editor.register(reteComponent);
    this.engine.register(reteComponent);
    return reteComponent;
  }

  reset() {
    this.nodeOrder = [];
  }

  addOnGraphChange(fn: (code: string) => void) {
    this.listeners.push(fn);
  }

  // eslint-disable-next-line class-methods-use-this
  build() {
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    return new Promise<void>((resolve) => {
    });
  }
}

// class SparkVPL<P extends Platform, L extends P['languages'][string]> {
//   graph: MyNode[] = [];
//
//   applicationBuilder!: ApplicationBuilder<P, L>;
//
//   constructor(readonly platform: P, readonly language: L) {
//     this.applicationBuilder = new ApplicationBuilder(platform, language);
//   }
//
//   addNode(node: MyNode) {
//     this.graph.push(node);
//   }
//
//   removeNode(node: MyNode) {
//     this.graph.splice(this.graph.indexOf(node), 1);
//   }
//
//   connect(node: MyNode, other: MyNode, socket: string) {
//     // eslint-disable-next-line no-param-reassign
//     node.outputNodes[socket] = other;
//   }
//
//   build() {
//     this.applicationBuilder.clear();
//     const code = (node: MyNode, args?: CodeArgs<any, any>) => {
//       const result = node.code({} as CodeArgs<any, any>);
//       if (node.outputNodes.flow) code(node.outputNodes.flow, args);
//     };
//     this.graph.forEach(code);
//     this.applicationBuilder.build();
//   }
// }
