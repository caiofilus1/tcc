import * as Rete from 'rete';
import { numberSocket, stringSocket } from '@/flow/sockets/number.socket';

export const flowSocket = new Rete.Socket('Flow');

export const serialSocket = new Rete.Socket('serial');

serialSocket.compatibleWith(numberSocket);
serialSocket.compatibleWith(stringSocket);
stringSocket.compatibleWith(serialSocket);
numberSocket.compatibleWith(serialSocket);
