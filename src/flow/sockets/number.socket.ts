import * as Rete from 'rete';

export const numberSocket = new Rete.Socket("Number");
export const stringSocket = new Rete.Socket("String");
export const booleanSocket = new Rete.Socket('Boolean');
export const voidSocket = new Rete.Socket('Void');
export const numberStringSocket = new Rete.Socket("number,string")
export const numberStringBooleanSocket = new Rete.Socket("number,string,boolean")

numberSocket.combineWith(numberStringBooleanSocket);
stringSocket.combineWith(numberStringBooleanSocket);
booleanSocket.combineWith(numberStringBooleanSocket);
