import { Control, Emitter, NodeEditor } from 'rete';
import NumControlView from '@/flow/controlers/NumControlView.vue';
import TextControlView from '@/flow/controlers/TextControlView.vue';

export default class ReteTextControl extends Control {
  private props: { readonly?: boolean; ikey: string; emitter: Emitter<unknown> };

  private component: unknown;

  private vueContext: {value: string} = { value: '' };

  constructor(emitter: NodeEditor, key: string, readonly?: boolean) {
    super(key);
    this.component = TextControlView;
    this.props = { emitter, ikey: key, readonly };
  }

  setValue(val: string) {
    this.vueContext.value = val;
  }

  getValue() {
    return this.vueContext.value;
  }
}
