import { Control, Emitter, NodeEditor } from 'rete';
import SelectControlView from '@/flow/controlers/SelectControlView.vue';

interface ReteSelectControlOptions {
  items: { label: string; value: string }[],
  placeholder?: string,
  defaultValue?: string | number,
}


export default class ReteSelectControl extends Control {
  private props: {
    readonly?: boolean;
    defaultValue?: string | number | undefined,
    ikey: string; emitter: Emitter<unknown>,
    options: {label: string, value: string}[],
    placeholder: string,
  };

  private component: unknown;

  private vueContext: {value: string} = { value: "" };

  constructor(emitter: NodeEditor, key: string, options: ReteSelectControlOptions) {
    super(key);
    this.component = SelectControlView;
    this.props = {
      emitter,
      ikey: key,
      defaultValue: options.defaultValue || "",
      options: options.items,
      placeholder: options.placeholder || ""
    };
  }

  setValue(val: string) {
    this.vueContext.value = val;
  }
}
