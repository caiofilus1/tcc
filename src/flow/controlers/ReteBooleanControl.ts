import { Control, Emitter, NodeEditor } from 'rete';
import SelectControlView from '@/flow/controlers/SelectControlView.vue';

export default class ReteBooleanControl extends Control {
  private props: {
    readonly?: boolean;
    defaultValue?: string,
    ikey: string; emitter: Emitter<unknown>,
    options: {label: string, value: string}[],
    placeholder: string,
  };

  private component: unknown;

  private vueContext: {value: number} = { value: 0 };

  constructor(emitter: NodeEditor, key: string) {
    super(key);
    this.component = SelectControlView;
    this.props = {
      emitter,
      ikey: key,
      defaultValue: 'false',
      options: [{ label: 'Ligar', value: 'true' }, { label: 'Desligar', value: 'false' }],
      placeholder: '',
    };
  }

  setValue(val: number) {
    this.vueContext.value = val;
  }

  getValue() {
    return this.vueContext.value;
  }
}
