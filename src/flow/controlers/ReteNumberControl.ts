import { Control, Emitter, NodeEditor } from 'rete';
import NumControlView from '@/flow/controlers/NumControlView.vue';

export default class ReteNumberControl extends Control {
  private props: { defaultValue?: number, readonly?: boolean; ikey: string; emitter: Emitter<unknown> };

  private component: unknown;

  private vueContext: {value: number} = { value: 0 };

  constructor(emitter: NodeEditor, key: string, options: {readonly?: boolean, defaultValue?: number}) {
    super(key);
    this.component = NumControlView;
    this.props = { emitter, ikey: key, readonly: !!options.readonly, defaultValue: options.defaultValue };
  }

  setValue(val: number) {
    this.vueContext.value = val;
  }
  getValue() {
    return this.vueContext.value;
  }
}
