export interface LanguageTypes {
    number: string,
    string: string,
    boolean: string,
    void: string,
}

export type Types = keyof LanguageTypes;
