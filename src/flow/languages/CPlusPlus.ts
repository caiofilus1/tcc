import {
  CallProps, FlowTemplate, FunctionProps, IfProps, ProcedureProps, ReturnProps, VarProps, WhileProps, WriteDigitalPinProp,
} from '@/flow/languages/Language';
import { Types } from '@/flow/languages/LanguageTypes';
import Platform from '@/flow/Platforms/Platform';

export default abstract class CppTemplate implements FlowTemplate {
  name = 'C++';

    abstract print(s: string): string

    abstract initPrint(opts: {speed: number}): string

    abstract registerPin(opts: { pin: Platform['pins'][number], direction: 'IN' | 'OUT'}): string;

    abstract readDigitalPin(pin: string): string;

    abstract readAnalogPin(pin: string): string;

    abstract writeDigitalPin(props: WriteDigitalPinProp): string;

    abstract setPin(pin: string, value: 'IN' | 'OUT'): string

    abstract delay(millis: number): string

    types = {
      number: 'float',
      string: 'char[]',
      boolean: 'boolean',
      void: 'void',
    };

    comparators = {
      equal: '==',
      greaterThan: '>',
      lessThan: '<',
      greaterOrEqual: '>=',
      different: '!=',
    };

    operations = {
      sum: '+',
      minus: '-',
      multiply: '*',
      divide: '/',
    };

    functionTemplate<T extends Types>(props: FunctionProps<T>) {
      const args = props.args.map((arg) => `${this.types[arg.type]} ${arg.name}`).join(', ');
      const code = props.code.split('\n').join('\n    ');
      return `${props.returnType} ${props.name}(${args}){
    ${code}
}
`;
    }

    varTemplate(props: VarProps) {
      if (props.name && !props.type) {
        return `${props.name} = ${props.value};`;
      } if (props.name && props.type) {
        return `${this.types[props.type]} ${props.name} = ${props.value};`;
      }
      return `${this.types[props.type as Types]} ${props.name};`;
    }

    returnTemplate<T extends Types>(props: ReturnProps<T>) {
      return `return ${props.value}`;
    }

    callTemplate(props: CallProps) {
      const args = props.args.map((arg) => `${'flow' in arg ? arg.name : arg.value}`).join(', ');
      if (!props.type) {
        return `${props.name} = ${props.functionName}(${args});`;
      }
      return `${this.types[props.type]} ${props.name} = ${props.functionName}(${args});`;
    }

    callProcedureTemplate<T>(props: ProcedureProps): string {
      const args = props.args.map((arg) => `${arg.name}`).join(', ');
      return `${props.functionName}(${args});`;
    }

    ifTemplate(props: IfProps) {
      const code = props.code.split('\n').join('\n    ');
      return `if(${props.left} ${this.comparators[props.comparator]} ${props.right}) {
    ${code}
}`;
    }

    elseTemplate(props: {code: string}) {
      const code = props.code.split('\n').join('\n    ');
      return `else {
    ${code}
}`;
    }

    whileTemplate(props: WhileProps) {
      const code = props.code.split('\n').join('\n    ');
      return `while(${props.left} ${this.comparators[props.comparator]} ${props.right}) {
    ${code}
}`;
    }
}
