// eslint-disable-next-line import/no-cycle
import CodeBuilder from '@/flow/codeBuilder/CodeBuilder';
import { Types } from '@/flow/languages/LanguageTypes';

export interface Variable {
    name: string,
    type: Types,
    deep?: number
    flow: CodeBuilder,
    flows: CodeBuilder[],
}

export interface Constant {
    name: string,
    type: Types,
    value: string,
}
