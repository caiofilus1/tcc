import { LanguageTypes, Types } from '@/flow/languages/LanguageTypes';
import { Constant, Variable } from '@/flow/languages/Variable';
import Platform from '@/flow/Platforms/Platform';

interface MathOperations {
    sum: string,
    minus: string,
    multiply: string,
    divide: string,
}

export interface Comparators {
    equal: string,
    greaterThan: string,
    lessThan: string,
    greaterOrEqual: string,
    different: string,
}

export type Comparator = keyof Comparators;

export interface Arg<T extends Types> {
    name: string
    type: T
}

export type Args<T extends Types[]> = { [K in keyof T]: Arg<T[K]> };

export type InferLanguageType<T> = T extends 'number'
    ? number: T extends 'string'
        ? string: T extends 'boolean'
            ? boolean: T extends 'void'
                ? void: string

export interface VarProps {
    name: string,
    type: Types,
    value: string
}

export interface SumProps {
    name: string,
    vars: string[]
}

export interface ReturnProps<T> {
    value: T
}

export interface FunctionProps<T extends Types = Types> {
    name: string
    returnType: T
    args: Array<Arg<Types>>
    code: string,
}

export interface CallProps extends Omit<VarProps, 'value'>{
    functionName: string
    args: Array<Variable | Constant>
}

export interface ProcedureProps {
    functionName: string
    args: Array<Variable | Constant>
}

export interface IfProps {
    left: string,
    right: string,
    comparator: keyof Comparators
    code: string,
}
export interface WhileProps {
    left: string
    right: string,
    comparator: keyof Comparators
    code: string,
}

export abstract class LanguageTemplate {
    abstract name: string

    abstract types: LanguageTypes

    abstract operations: MathOperations

    abstract comparators: Comparators

    abstract functionTemplate: (varProps: FunctionProps) => string

    abstract varTemplate: (varProps: VarProps) => string

    abstract returnTemplate: <T extends Types>(returnProps: ReturnProps<T>) => string

    abstract callTemplate: (varProps: CallProps) => string

    abstract callProcedureTemplate: (varProps: ProcedureProps) => string

    abstract ifTemplate: (varProps: IfProps) => string

    abstract elseTemplate: (elseProps: {code: string}) => string

    abstract whileTemplate: (varProps: WhileProps) => string
}

export interface WriteDigitalPinProp {
    pin: string,
    value: boolean,
}

export abstract class FlowTemplate extends LanguageTemplate {
  abstract print: (s: string) => string

  abstract initPrint: (opts: {speed: number}) => string

  abstract setPin(pin: string, value: 'IN' | 'OUT'): string

  abstract registerPin(opts: { pin: Platform['pins'][number] | Platform['analogPins'][number], direction: 'IN' | 'OUT'}): string

  abstract readDigitalPin(pin: string): string

  abstract readAnalogPin(pin: string): string

  abstract writeDigitalPin(props: WriteDigitalPinProp): string

  abstract delay(millis: number): string
}
