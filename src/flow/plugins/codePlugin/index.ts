
import {Engine, Node, NodeEditor} from "rete";
import {Data, NodeData} from "rete/types/core/data";

function install(editor: NodeEditor, params: unknown) {}

function getVarName(node: NodeData) {
    return `${node.name}${node.id}`;
}

export async function generate(engine: Engine, data: Data) {
    let file = '';

    engine = engine.clone();

    Array.from(engine.components.values()).forEach(c => {
        c = Object.assign(Object.create(Object.getPrototypeOf(c)), c)

        c.worker = (node, inputs, outputs) => {
            function add(name: string, expression: string) {
                if (!expression) {
                    file += `${name};\n`;
                    return;
                }

                const varName = `${getVarName(node)}${name}`;

                file += `const ${varName} = ${expression};\n`;
                outputs[name] = varName;
            }
            if("code" in c) { // @ts-ignore
                (c as unknown).code(node, inputs, add);
            }
        }
        c.worker.bind(c);

        engine.components.set(c.name, c);
    })

    await engine.process(data);
    return file;
}

export default {
    install
}