import { createPinia } from 'pinia';
import './theme.store';

const pinia = createPinia();

export default pinia;
