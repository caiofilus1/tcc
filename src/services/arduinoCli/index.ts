import electron from 'electron';
import util from 'util';
import path from 'path';
// import _yamljs from "yamljs";
import os from 'os';
import fs from 'fs';
// eslint-disable-next-line camelcase
import ChildProcess from 'child_process';

const exec = util.promisify(ChildProcess.exec);
const writeFile = util.promisify(fs.writeFile);

const command = {
  board: {
    attach: 'board attach',
    details: 'board details -b',
    list: 'board list',
    listall: 'board listall',
  },
  burnBootloader: 'burn-bootloader',
  cache: {
    clean: 'cache clean',
  },
  compile: 'compile',
  completion: 'completion',
  config: {
    dump: 'config dump',
    init: 'config ini',
  },
  core: {
    download: 'core download',
    install: 'core install',
    list: 'core list',
    search: 'core search',
    uninstall: 'core uninstall',
    update: {
      index: 'core update-index',
    },
    upgrade: 'core upgrade',
  },
  daemon: 'daemon',
  debug: 'debug',
  lib: {
    deps: 'lib deps',
    download: 'lib download',
    install: 'lib install',
    list: 'lib list',
    search: 'lib search',
    uninstall: 'lib uninstall',
    updateIndex: 'lib update-index',
    upgrade: 'lib upgrade',
  },
  outdated: 'outdated',
  sketch: {
    new: 'sketch new',
  },
  update: 'update',
  upgrade: 'upgrade',
  upload: 'upload',
  version: 'version',
};

const SO = new Map<string, string>([
  ['linux', 'arduino-cli'],
  ['win', 'arduino-cli.exe'],
  ['darwin', 'arduino-cli'],
]);

const osPlataform = os.platform();
const arduinoFolder = 'arduino';
const arduinoCliPath = path.join(arduinoFolder, SO.get(osPlataform) || 'arduino-cli.exe');

export default class ArduinoCli {
  constructor(readonly opts: {board: string, port: string}) {}

  async run(code: string) {
    const sketchFileName = 'arduino.ino';
    const filePath = path.join(arduinoFolder, sketchFileName);
    await writeFile(filePath, code);
    await exec(
      `${arduinoCliPath} compile ${filePath} -b ${this.opts.board} -p ${this.opts.port}`,
    );
    const execRes = await exec(
      `${arduinoCliPath} upload ${filePath} -b ${this.opts.board} -p ${this.opts.port}`,
    );
    return execRes.stdout;
  }

  static async listBoards(): Promise<ArduinoCli[]> {
    const execRes = await exec(`${arduinoCliPath} ${command.board.list}`);
    if (!execRes.stdout.startsWith('Port Protocol Type')) return [];
    let rows = execRes.stdout.split('\n');
    rows = rows.slice(1, rows.length - 2);
    return rows.map((item) => {
      const boardAttrs = item.split(' ');
      const port = boardAttrs[0];
      const board = boardAttrs[boardAttrs.length - 1] || 'arduino:avr:uno';
      return new ArduinoCli({ board, port });
    });
  }
}
