import { defineComponent } from 'vue';

const ValidationMixin = defineComponent({
  data() {
    const re = /\S+@\S+\.\S+/;
    const jaRegex = /^[\u30A0-\u30FF]+$/;
    const validPass = /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!%&@#$^*?_~,:;\][]{8,32}$/;
    return {
      defaultRules: {
        email: (value: string) => re.test(value) || '無効なメールアドレス',
        password: (value: string) => validPass.test(value)
          || '半角英字の大文字、小文字、数字を1文字以上使用した8～32文字のパスワードを入力してください',
        required: (value: string) => !!value || 'この項目は必須です',
        number: (value: string) => !Number.isNaN(parseFloat(value)) || '数 字を入力してください',
        cpf: (value: string) => value.length === 11 || 'ERRORS.cpf',
        cnh: (value: string) => value.length === 11 || 'ERRORS.cnh',
        max(maxValue: number | string) {
          return (value: string) => {
            if (!value) return true;
            return (
              value.length <= parseFloat(maxValue as string) || '入力文字列が長過ぎます'
            );
          };
        },
        min(minValue: number | string) {
          return (value: string) => {
            if (!value) return true;
            return value.length >= parseFloat(minValue as string) || '郵便番号を正しく入力してください。';
          };
        },
        isSame(email: string) {
          return (value: string) => value === email || 'メールアドレスが上記と一致しません';
        },
        isKana: (value: string) => jaRegex.test(value) || '全角カナで入力して下さい',
        onlyFullWight: (value: string) => this.detectFullWidth(value) || 'この項目は必須です',
      },
    };
  },
  methods: {
    detectFullWidth(value: string): boolean {
      if (!value) return false;
      return [...value].some((char) => {
        const code = char.charCodeAt(0);
        return (code >= 0x2000 && code <= 0xff60) || code >= 0xffa0;
      });
    },
  },
});
export default ValidationMixin;
