import {
  defineProps,
  inject, onMounted, onUnmounted, Ref,
} from 'vue';

export type ValidatorRule<T> = (value: T) => string | boolean;

export interface FormField<T = unknown> {
  el: HTMLElement;
  value: T;
  rules: Array<(value: T) => boolean | string>;
}

export interface FormProvider<T = unknown> {
  registerInput: (item: FormField<T>) => void;
  unregisterInput: (el: HTMLElement) => void;
  updateInput: (el: HTMLElement, value: T) => void;
}

const emailRegex = /\S+@\S+\.\S+/;
const passwordRegex = /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!%&@#$^*?_~,:;\][]{8,32}$/;
const jaRegex = /^[\u30A0-\u30FF]+$/;

function detectFullWidth(value: string): boolean {
  if (!value) return false;
  return [...value].some((char) => {
    const code = char.charCodeAt(0);
    return (code >= 0x2000 && code <= 0xff60) || code >= 0xffa0;
  });
}

// eslint-disable-next-line import/prefer-default-export
export function useValidator() {
  return {
    validate<T>(rules: ValidatorRule<T>[], value: T) {
      let error = '';
      rules.some((rule) => {
        const res = rule(value);
        if (typeof res === 'string') error = res;
        return error;
      });
      return error;
    },
    rules: {
      required: (value: string) => value.length > 0 || 'この項目は必須です',
      email: (value: string) => emailRegex.test(value) || '無効なメールアドレス',
      password: (value: string) => passwordRegex.test(value) || '無効なパスワード',
      cpf: (value: string) => value.length === 11 || 'ERRORS.cpf',
      cnh: (value: string) => value.length === 11 || 'ERRORS.cnh',
      max(maxValue: number | string) {
        return (value: string) => {
          if (!value) return true;
          return (
            value.length <= parseFloat(maxValue as string) || '入力文字列が長過ぎます'
          );
        };
      },
      min(minValue: number | string) {
        return (value: string) => {
          if (!value) return true;
          return value.length >= parseFloat(minValue as string) || '郵便番号を正しく入力してください。';
        };
      },
      isSame(email: string) {
        return (value: string) => value === email || 'メールアドレスが上記と一致しません';
      },
      isKana: (value: string) => jaRegex.test(value) || '全角カナで入力して下さい',
      onlyFullWight: (value: string) => detectFullWidth(value) || 'この項目は必須です',
    },
  };
}

export function useForm<T = unknown>(inputRef: Ref<HTMLElement | undefined>, rules: ValidatorRule<T>[], defaultValue: T) {
  const validator = useValidator();
  const form = inject('form', null) as FormProvider<T> | null;

  function updateFormValidation(value: T) {
    if (form && inputRef.value) form.updateInput(inputRef.value, value);
    return validator.validate(rules, value);
  }

  onMounted(() => {
    if (form && inputRef.value) {
      form.registerInput({ el: inputRef.value, value: defaultValue, rules });
    }
  });

  onUnmounted(() => {
    if (form && inputRef.value) {
      form.unregisterInput(inputRef.value);
    }
  });

  return {
    updateFormValidation,
  };
}
