import { useRouter } from 'vue-router';

// eslint-disable-next-line import/prefer-default-export
export function useWindow() {
  const router = useRouter();

  function open(routeName: string) {
    const routeData = router.resolve({ name: routeName });
    window.open(routeData.href);
  }
  function openInNewTab(routeName: string) {
    const routeData = router.resolve({ name: routeName });
    window.open(routeData.href, '_blank');
  }

  return {
    open,
    openInNewTab,
  };
}
