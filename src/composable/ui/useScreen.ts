import { computed, onMounted, ref } from 'vue';

export interface BreakpointOptions {
  xs?: number;
  sm?: number;
  md?: number;
  lg?: number;
  xl?: number;
}

// eslint-disable-next-line import/prefer-default-export
export function useBreakpoint(options?: BreakpointOptions) {
  const {
    xs: defaultXs = 600,
    sm: defaultSm = 960,
    md: defaultMd = 1264,
    lg: defaultLg = 1904,
  } = options || {};
  const width = ref(window.innerWidth);

  function onResize() {
    width.value = window.innerWidth;
  }

  const xs = computed(() => width.value < defaultXs);
  const sm = computed(() => width.value < defaultSm);
  const md = computed(() => width.value < defaultMd);
  const lg = computed(() => width.value < defaultLg);
  const xl = computed(() => width.value >= defaultLg);
  const xsOnly = computed(() => xs.value && !sm.value);
  const smOnly = computed(() => sm.value && !md.value);
  const smAndDown = computed(() => sm.value && !lg.value);
  const smAndUp = computed(() => xs.value && !md.value);
  const mdOnly = computed(() => md.value && !lg.value);
  const mdAndDown = computed(() => md.value && !xl.value);
  const mdAndUp = computed(() => sm.value && !lg.value);
  const lgOnly = computed(() => lg.value && !xl.value);
  const lgAndDown = computed(() => lg.value);
  const lgAndUp = computed(() => md.value && !xl.value);
  const xlOnly = computed(() => xl.value);

  const name = computed(() => {
    if (xs.value) return 'xs';
    if (sm.value) return 'sm';
    if (md.value) return 'md';
    if (lg.value) return 'lg';
    return 'xl';
  });

  onMounted(() => {
    window.addEventListener('resize', onResize);
  });

  return {
    name,
    xs,
    sm,
    md,
    lg,
    xl,
    xsOnly,
    smOnly,
    smAndDown,
    smAndUp,
    mdOnly,
    mdAndDown,
    mdAndUp,
    lgOnly,
    lgAndDown,
    lgAndUp,
    xlOnly,
  };
}
